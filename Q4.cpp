/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>
#include <string>
#include <iostream>
#include <stdint.h>

using namespace std;

string ConcatRemove(string s, string t, int k)
{
    // Based on constraints all numbers should fit in a 8-bit unsigned variable
    // Holds the number of concats needed for s to become t
    uint8_t toConcat = 0;
    // Holds the number of removes needed for s to become t
    uint8_t toRemove = 0;
    // Holds the total number of operations
    uint8_t totalOperations = 0;
    bool result = false;
    
    // String index and also the number of common characteres 
    int i = 0;
    for(i=0; i < s.length() && i < t.length(); i++)
    {
        // In the first umcommon character
        if(s[i] != t[i])
        {
            // Get out of here
            break;
        }
    }
    
    // Find out how many letters to remove from s
    toRemove = s.length() - i;
    // Find out how many letters to concat on s
    toConcat = t.length() - i;
    // Total operation
    totalOperations = toConcat + toRemove;
    // If number of available operations is less than total needed
    if(k < totalOperations)
    {
        // Then it is always impossible to make t into s
        result = false;
    }
    else
    {
        // If k is greater than needed operations + common_chars * 2
        // It is possible to remove all characters and decrement k until
        // it is exaclty what was needed + common_chars
        if(k >= (totalOperations + i*2))
        {
            result = true;
        }
        // If k - needed operations is an even number, 
        // It can be decremented to the correct operations combining remove and concat operations
        else if(((k - totalOperations) % 2) == 0)
        {
            result = true;
        }
        // We tried everything
        else
        {
            // Not possible at all
            result = false;
        }
    }
    
    // Return "Yes" if s can become t with k remove/concat operations
    return (result ? "Yes" : "No");
}

int main()
{
    string s;
    string t;
    int k;
    
    // Infinte loop so I can test until I get tired
    while(1)
    {
    cout << "Please, enter with string s: ";
    cin >> s;
    cout << "Please, enter with string t: ";
    cin >> t;
    cout << "Please, enter number of operations k: ";
    cin >> k;
    
    cout << ConcatRemove(s,t,k) << endl;;

    }
    return 0;
}